﻿
namespace Class
{
    class UniDemo
    {
        public static void Main(string[] args)
        {
            University uni = new University(2, 15);

            uni.AddProfessor(new Professor("P1", 50, "Math", 20));
            uni.AddProfessor(new Professor("P2", 40, "English", 15));
            
            uni.AddStudent(new Student("S1", 19, "A0256", 1, "Math"));
            uni.AddStudent(new Student("S2", 21, "B1302", 3, "English"));
            uni.AddStudent(new Student("S3", 20, "B1552", 2, "English"));

            System.Console.WriteLine("The number of occupants in University: " + uni.Occupants);
            System.Console.WriteLine("The number of Rooms in University: " + uni.Rooms);
            System.Console.WriteLine("The number of floors in University building: " + uni.Floors);
            System.Console.WriteLine("The number of Professors in University: " + uni.ProfessorCount);
            System.Console.WriteLine("The number of Students in University: " + uni.StudentsCount);

        }
    }
}
