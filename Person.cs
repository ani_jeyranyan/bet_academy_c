﻿
namespace Class
{
    class Person
    {
        private string name;
        private int age;

        public Person()
        {
            name = "No Name!";
            age = 0;
        }

        public Person(string name, int age)
        {
            this.name = name;
            if(age > 0 && age < 100)
            {
                this.age = age;
            }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get { return age; }
            set
            {
                if (age > 0 && age < 100)
                    age = value;
            }
        }
    }
}
