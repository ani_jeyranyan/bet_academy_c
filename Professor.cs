﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Class
{
    class Professor : Person
    {
        private string profession;
        private int experience;

        public Professor() : base()
        {
            profession = "Not mentioned!";
            experience = 0;
        }

        public Professor(string name, int age, string profession, int experience) : base(name, age)
        {
            this.profession = profession;
            this.experience = experience;
        }

        public string Profession
        {
            get { return profession; }
            set { profession = value; }
        }

        public int Experience
        {
            get { return experience; }
            set
            {
                if(value > 0)
                {
                    experience = value;
                }
            }
        }


    }
}
