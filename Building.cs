﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Class
{
    class Building
    {
        private int floors;
        private int rooms;
        private int occupants;

        public Building() { }

        public Building(int floors, int rooms, int occupants)
        {
            this.floors = floors;
            this.rooms = rooms;
            this.occupants = occupants;
        }

        public int Floors
        {
            get { return floors; }
            set
            {
                if(value > 0)
                {
                    floors = value;
                }
            }
        }

        public int Rooms
        {
            get { return rooms; }
            set
            {
                if (value > 0)
                {
                    rooms = value;
                }
            }

        }

        public int Occupants
        {
            get { return occupants; }
            set
            {
                if (value > 0)
                {
                    occupants = value;
                }
            }
        }
    }
}
