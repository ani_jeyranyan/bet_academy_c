﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubString
{
    class Program
    {
        static void Main(string[] args)
        {
            //Exercise1
            //Print all the substrings in the given string and give the total count.
            Console.WriteLine("The total number of substrings is  " + allSubStrings("hello", 0, 0));

            //Exercise2
            //Print the indexes, from which the given substring is present int the given string
            findSubstring("HelloHello", "ello");
        }

        public static int allSubStrings(string str, int start, int end)
        {
            if (end > str.Length)
            {
                return 0;
            }

            int count = 0;
            for (int i = start; i < end; i++)
            {
                Console.Write(str[i]);
            }
            ++count;
            Console.WriteLine();
            if (start < end)
                return count += allSubStrings(str, ++start, end);
            else
                return allSubStrings(str, 0, ++end);
        }

        public static void findSubstring(string str, string sbstr)
        {
            int strSize = str.Length;
            int sbstrSize = sbstr.Length;
            int i = 0;
            while (i < strSize)
            {
                int answer = str.IndexOf(sbstr, i);
                if (answer == -1)
                    return;
                else
                {
                    Console.WriteLine("Found in index " + answer);
                    i = answer + sbstrSize;
                }

            }
        }
    }
}
