﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Class
{
    class University : Building
    {
        private static int maxStudents = 1000; //max number of students that can study in university
        private static int maxProfessors = 30;  //30 is max number of professors that can work in university

        private int currentStudents = 0; //current number of students in uni
        private int currentProfessors = 0; //current number of professors in uni

        Student[] students = new Student[maxStudents];
        Professor[] professors = new Professor[maxProfessors]; 

        public University() : base() { }

        public University(int floors, int rooms) : base(floors, rooms, 0) { }

        public void AddStudent(Student s)
        {
            if(currentStudents < maxStudents)
            {
                students[currentStudents++] = s;
                ++base.Occupants;
            }
            //in else case it will throw an exception, 
            //I will add it later as I didn't manage to learn the syntax for exceptions and the types in C#
        }

        public void AddProfessor(Professor p)
        {
            if (currentProfessors < maxProfessors)
            {
                professors[currentProfessors++] = p;
                ++base.Occupants;
            }
            //else throw an exception 
        }

        public int ProfessorCount
        {
            get { return currentProfessors; }
        }

        public int StudentsCount
        {
            get { return currentStudents; }
        }


    }
}
