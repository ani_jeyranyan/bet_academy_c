﻿using System.Collections.Generic;
using System.Text;

namespace Class
{
    class Student : Person
    {
        private string studentID;
        private int yearOfStudy;
        private string profession;

        public Student() : base()
        {
            studentID = "Unknown";
            profession = "Not mentioned!";
        }
        public Student(string name, int age, string studentID, int yearOfStudy, string profession) : base(name, age)
        {
            this.studentID = studentID;
            if (yearOfStudy > 0 && yearOfStudy < 5)
            {
                this.yearOfStudy = yearOfStudy;
            }

            this.profession = profession;

        }

        public string StudentID
        {
            get { return studentID; }
            set { studentID = value; }
        }

        public int YearOfStudy
        {
            get { return yearOfStudy; }
            set
            {
                if (value > 0 && value < 5)
                {
                    yearOfStudy = value;
                }
            }
        }

        public string Profession
        {
            get { return profession; }
            set { profession = value; }
        }

    }
}
